<?php

namespace Models;

use DBadapters\PDOadapter;
use PDO;

class Twitts
{
    private $conn = null;

    public function __construct()
    {
        $pdo = new PDOadapter();
        $this->conn = $pdo->getConnection();
    }

    public function save($twitts) {

        foreach($twitts as $twitt){
            try{
                $this->conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
                $sql = "INSERT INTO twitts (created_at,twitt_id,text,user_id,user_name,profile_image_url,screen_name)
					    VALUES ( :created_at, :twitt_id, :text, :user_id, :user_name, :profile_image_url, :screen_name);";

                $stmt = $this->conn->prepare( $sql );
                $stmt->bindValue( "created_at", date("Y-m-d H:i:s", strtotime($twitt->created_at)), PDO::PARAM_INT );
                $stmt->bindValue( "twitt_id", $twitt->id, PDO::PARAM_INT );
                $stmt->bindValue( "text", $twitt->text, PDO::PARAM_STR );
                $stmt->bindValue( "user_id", $twitt->user->id, PDO::PARAM_INT );
                $stmt->bindValue( "user_name", $twitt->user->name, PDO::PARAM_STR );
                $stmt->bindValue( "profile_image_url", $twitt->user->profile_image_url, PDO::PARAM_STR );
                $stmt->bindValue( "screen_name", $twitt->user->screen_name, PDO::PARAM_STR );
                $stmt->execute();

            }
            catch (PDOException $e) {
                echo $e->getMessage();
            }
        }

        $this->conn = null;
    }

    public function get() {

        try{
            $this->conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
            $sql = "SELECT id,created_at,twitt_id,text,user_name,user_id,profile_image_url,screen_name
                    FROM twitts
                    ORDER BY created_at DESC;";

            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
            $twitts = $stmt->fetchAll(PDO::FETCH_OBJ);

            $this->conn = null;

            return $twitts;
        }
        catch (PDOException $e) {
            echo $e->getMessage();
        }

    }

    public function getLastEntry() {

        try{
            $this->conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
            $sql = "SELECT max(created_at) last_entry FROM twitts;";

            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetch(PDO::FETCH_OBJ);

            if ($result){
                $result = $result->last_entry;
            }

            return $result;
        }
        catch (PDOException $e) {
            echo $e->getMessage();
        }

    }
}