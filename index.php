<?php

//show notices and errors
error_reporting(E_ALL);
ini_set("display_errors", 1);

//load config data
require "config.php";

//autoload classes for TwitterOAuth package
require "vendor/autoload.php";

//autoload classes
function autoloadClasses($className) {
    $file = str_replace('\\',DIRECTORY_SEPARATOR,$className);
    include "{$file}.php";
}
spl_autoload_register("autoloadClasses");

use Controllers\Index;

$mainController = new Index();
$mainController->render();

