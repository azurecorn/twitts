# Some job interview task #

## Task - Show Twitts ##

1. Create a Class with the following mandatory methods.
• Connect to the Twitter REST API and Call statuses (tweets).
• Update the database with data from the API response.
• And additional methods of your choice and need.
2. Create a MySQL DB with a logical structure to save and easily access the twitter data.
3. Create a cron.php to update the database with data from https://twitter.com/GEL_prep
4. Create a index file to show the cached tweets with possibility to answer, retweet and favorite each tweet. Design the page in a general ekomi like design of your choice.
 
## Install note ##

*   git clone repo
*   composer install
*   setup virtual host
*   setup proper file permissions for project
*   import dump.sql
*   setup DB credentials in config.php
*   setup example cronjob: */1 * * * * /usr/bin/php /var/www/twitts/cron.php

### Main Technology ###
plain OO PHP