<?php

namespace DBadapters;

use PDO;

class PDOadapter
{
    private $conn = null;

    public function __construct()
    {
        try{
            $this->conn = new PDO( 'mysql:host='.getenv('DB_SERVER').';dbname='.getenv('DB_DATABASE'), getenv('DB_USERNAME'), getenv('DB_PASSWORD'));
        }
        catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function getConnection()
    {
        return $this->conn;
    }


}
