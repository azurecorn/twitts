-- Dumping database structure for gel
CREATE DATABASE IF NOT EXISTS `gel` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `gel`;


-- Dumping structure for table gel.twitts
CREATE TABLE IF NOT EXISTS `twitts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `twitt_id` bigint(20) NOT NULL,
  `text` varchar(255) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_name` varchar(128) NOT NULL,
  `profile_image_url` varchar(255) NOT NULL,
  `screen_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;