<?php

namespace Controllers;

use Abraham\TwitterOAuth\TwitterOAuth;
use Models\Twitts;
use Helpers\ViewHelper;

class Index
{
    public function render()
    {
        $twittsObj = new Twitts();

        //prepare variables for view template
        $view_twitts = $twittsObj->get();
        $view_Helper = new ViewHelper();

        include("views/index.php");

    }

    public function insert()
    {
        $new_twitts = array();

        $tw_connection = new TwitterOAuth(getenv('CONSUMER_KEY'), getenv('CONSUMER_SECRET'), getenv('ACCESS_TOKEN'), getenv('ACCESS_TOKEN_SECRET'));

        $twitts = $tw_connection->get('statuses/user_timeline', array('screen_name' => getenv('TW_ACCOUNT'), 'exclude_replies' => 'true', 'include_rts' => 'false'));

        $twittsObj = new Twitts();
        $last_entry = $twittsObj->getLastEntry();

        //DB already has some entries
        if(!is_null($last_entry)){
            $last_entry_dt = new \DateTime($last_entry);
            foreach($twitts as $twitt){
                if(new \DateTime($twitt->created_at) > $last_entry_dt){
                    $new_twitts [] = $twitt;
                }
            }
        }
        //DB is empty
        else{
            $new_twitts = $twitts;
        }

        $twittsObj->save($new_twitts);

    }
}