<?php

namespace Helpers;

class ViewHelper
{
    public function formatTwitterText($tweetText)
    {
        // Make links active
        $tweetText = preg_replace("/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/", '<a href=\"\\0\">\\0</a>', $tweetText);

        // Linkify user mentions
        $tweetText=preg_replace('/(^|[^a-z0-9_])@([a-z0-9_]+)/i', '$1<a href="http://twitter.com/$2">@$2</a>', $tweetText);

        //Linkify tags
        $tweetText= preg_replace("/\#(\w+)/", '<a href="http://twitter.com/search?q=$1" target="_blank">#$1</a>',$tweetText);

        return $tweetText;
    }

    public function formatDate($date)
    {
        $date = strtotime($date);
        $result =  date('d-M', $date);

        return $result;
    }
}