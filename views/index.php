<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="GEL Twitts Application">
    <meta name="author" content="Milan Nikolic">

    <title>GEL Twitts</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <!-- Custom CSS -->
    <link href="public/css/custom.css" rel="stylesheet">

</head>
<body>
<!-- Navigation -->
<nav class="navbar navbar-default navbar-custom navbar-fixed-top">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">GEL Task</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="#">About Us</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<!-- Page Header -->
<header class="intro-header">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="site-heading">
                    <h1>GEL Twitts</h1>
                    <hr class="small">
                    <span class="subheading">Listing Twitts App</span>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Main Content -->

<div class="container">
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
            <?php foreach($view_twitts as $twitt):?>
            <div class="post-preview row">
                <div class="col-lg-1">
                    <a href="https://twitter.com/<?php echo $twitt->screen_name?>">
                        <img src="<?php echo $twitt->profile_image_url?>"/>
                    </a>
                </div>
                <div class="col-lg-11">
                    <div class="twitt-header">
                        <a href="https://twitter.com/<?php echo $twitt->screen_name?>">
                            <span class="black">
                                <?php echo $twitt->user_name ?>@<?php echo $twitt->screen_name ?>
                            </span>
                        </a>
                        <span class="post-meta gray">
                            <?php echo $view_Helper->formatDate($twitt->created_at) ?>
                        </span>
                    </div>
                    <div>
                        <?php echo $view_Helper->formatTwitterText($twitt->text)?>
                    </div>
                    <div class="twitter_actions">
                        <a target="_blank" href="https://twitter.com/intent/tweet?in_reply_to=<?php echo $twitt->twitt_id?>">
                            <img src="public/img/reply-icon-16.png"/>
                        </a>
                        <a target="_blank" href="https://twitter.com/intent/retweet?tweet_id=<?php echo $twitt->twitt_id?>">
                            <img src="public/img/retweet-icon-16.png"/>
                        </a>
                        <a target="_blank" href="https://twitter.com/intent/like?tweet_id=<?php echo $twitt->twitt_id?>">
                            <img src="public/img/like-icon-16.png"/>
                        </a>
                    </div>
                </div>
            </div>
            <?php endforeach?>
            <hr>
        </div>
    </div>
</div>

<!-- Footer -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <ul class="list-inline text-center">
                    <li>
                        <a href="#">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                                </span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                                </span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-github fa-stack-1x fa-inverse"></i>
                                </span>
                        </a>
                    </li>
                </ul>
                <p class="copyright text-muted">Copyright &copy; GEL Twitter App 2016</p>
            </div>
        </div>
    </div>
</footer>

</html>
